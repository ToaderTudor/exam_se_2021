package exercise2;

import javax.swing.*;

public class Main extends JFrame {
    private JTextField text1;
    private JTextField text2;
    private JTextField count;
    private JButton add;

    public Main() {
        setTitle("Character Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        count.setText("0");
        add.addActionListener(e -> {
            int characters;

            characters = text1.getText().length() + text2.getText().length();
            count.setText(characters + "");
        });
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        text1 = new JTextField();
        text1.setBounds(50, 50, 80, 20);
        text2 = new JTextField();
        text2.setBounds(50, 100, 80, 20);
        count = new JTextField();
        count.setEditable(false);
        count.setBounds(50, 150, 80, 20);
        add = new JButton("Count");
        add.setBounds(50, 185, 80, 20);

        add(text1);
        add(text2);
        add(count);
        add(add);
    }

    public static void main(String[] args) {
        new Main();
    }
}

