package exercise1;

public class Main {
    public static void main(String[] args) {
        B b1 = new B();
        E e = new E();
        B b2 = new B(e);
        U u = new U();
        u.useB();
    }
}

class A {

}

class B extends A {
    private String param;
    private C c;
    private D d;
    private E e;

    public B() {
        param = "Exam";
        c = new C();
    }

    public B(E e) {
        param = "Exam";
        c = new C();
        this.e = e;
    }

    public void x() {
        System.out.println("Message: " + param);
    }

    public void y() {

    }
}

class C {

}

class D {

}

class E {

}

class U {
    public void useB() {
        B b = new B();
        b.x();
    }
}